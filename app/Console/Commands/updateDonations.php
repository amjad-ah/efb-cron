<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class updateDonations extends Command
{
    private $banks = [
        'FOODBANK_IN' => [
            'username' => 'merchant.FOODBANK_IN',
            'password' => '535b564432ce55db3bad2d413341e6b7'
        ],
        'FOODBANK_EGP' => [
            'username' => 'merchant.FOODBANK_EGP',
            'password' => '4f3df8de957664848c55bf16462f6d5f'
        ],
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'donation:update ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $from = $this->ask('Start date? "it should be formatted like \'yyyy-mm-dd\'"');
        $to = $this->ask('End date? "it should be formatted like \'yyyy-mm-dd\'"');

        $from = strtotime("{$from} 00:00:00");
        $to = strtotime("{$to} 23:59:59");

        $donations = DB::table('custom_migs_transactions')
            ->whereBetween('created', [$from, $to])
            ->get()
            ->groupBy('donor_id');

        foreach ($donations as $donorId => $donor) {
            $this->info("checking donation #{$donorId}");
            foreach ($donor as $donation) {
                if ($donation->vpc_message == 'Approved') {
                    DB::table('custom_migs_transactions')
                        ->where('donor_id', '=', $donorId)
                        ->where('vpc_merchtxnref', '!=', $donation->vpc_merchtxnref)
                        ->update(['vpc_message' => 'Canceled']);

                    continue 2;
                }
            }
            $donation = $this->checkDonation($donorId, $donor->first()->vpc_merchant);

            if ($donation->getStatusCode() != 200) {
                $this->warn("Donation not found #{$donorId}");
                DB::table('custom_migs_transactions')
                    ->where('donor_id', '=', $donorId)
                    ->update(['vpc_message' => 'Failed']);
            } else {
                try {
                    $donation = json_decode($donation->getBody()->getContents(), true);
                    $status = ($donation['status'] == 'CAPTURED') ? 'Approved' : $donation['status'];
                    $referenceNumber = $donation['transaction'][0]['transaction']['reference'];
                    DB::table('custom_migs_transactions')
                        ->where('donor_id', '=', $donorId)
                        ->where('vpc_merchtxnref', '=', $referenceNumber)
                        ->update([
                            'vpc_message' => $status
                        ]);

                    DB::table('custom_migs_transactions')
                        ->where('donor_id', '=', $donorId)
                        ->where('vpc_merchtxnref', '!=', $referenceNumber)
                        ->update([
                            'vpc_message' => 'Canceled'
                        ]);
                } catch (\Exception $e) {
                    $this->error("Donation error #{$e->getMessage()}");
                    continue;
                }
            }
        }

        $this->info(count($donations) . ' donations checked');
        return 0;
    }

    private function checkDonation($donationId, $merchant)
    {
        if (isset($this->banks[$merchant])) {
            $merchantData = $this->banks[$merchant];

            return $this->requestDonation($donationId, $merchant, $merchantData['username'], $merchantData['password']);
        }

        return null;
    }

    private function requestDonation($donationId, $merchant, $username, $password)
    {
        $client = new Client();
        $response = $client->request('GET',
            "https://nbe.gateway.mastercard.com/api/rest/version/58/merchant/{$merchant}/order/{$donationId}",
            ['auth' => [$username, $password]]
        );
        
        return $response;
    }
}
